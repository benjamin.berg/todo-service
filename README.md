TODO-SERVICE
============

SUMAMRY
-------

A simple TODO web service written in Kotlin utilizing Spring Boot, Postgres, and PG-Pool.

GETTING STARTED
---------------

1. Build the web service docker image: `sh ./build.sh`
2. Launch the web service and DB: `docker-compose up -d`
3. Use the `todo.http` file to interact with the web API.