#!/bin/sh

./mvnw package

docker build --build-arg JAR_FILE=target/todo-service-0.0.1-SNAPSHOT.jar -t benjaminjacobberg/todo-service:latest .
