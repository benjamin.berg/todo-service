/*
 * Copyright (c) Benjamin Jacob Berg <https://gitlab.com/benjamin.berg> and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package com.benjaminjacobberg.todoservice.todo

import org.springframework.data.domain.Example
import org.springframework.data.domain.ExampleMatcher
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
@Transactional
class TodoService(private val todoRepository: TodoRepository) {

    fun add(todoEntity: TodoEntity): TodoEntity = todoRepository.save(todoEntity)

    fun find(todoEntity: TodoEntity, pageable: Pageable): Page<TodoEntity> = todoRepository.findAll(Example.of(todoEntity, ExampleMatcher.matchingAny()), pageable)

    fun remove(id: UUID): Unit = todoRepository.deleteById(id)

}