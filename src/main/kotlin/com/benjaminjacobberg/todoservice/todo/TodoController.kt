/*
 * Copyright (c) Benjamin Jacob Berg <https://gitlab.com/benjamin.berg> and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package com.benjaminjacobberg.todoservice.todo

import org.springframework.dao.IncorrectResultSizeDataAccessException
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController
class TodoController(private val todoService: TodoService) {

    @PostMapping("/todo")
    fun create(@RequestBody todoEntity: TodoEntity): TodoEntity = todoService.add(todoEntity = todoEntity)

    @GetMapping("/todo")
    fun read(todoEntity: TodoEntity, pageable: Pageable): Page<TodoEntity> = todoService.find(todoEntity = todoEntity, pageable = pageable)

    @PutMapping("/todo")
    fun update(@RequestBody todoEntity: TodoEntity): TodoEntity = todoService.add(todoEntity = todoEntity)

    @DeleteMapping("/todo/{id}")
    fun delete(@PathVariable("id") id: UUID): Unit = todoService.remove(id = id)

    @ExceptionHandler(IncorrectResultSizeDataAccessException::class)
    fun handleIncorrectResultSizeDataAccessException(exception: IncorrectResultSizeDataAccessException): ResponseEntity<String> = ResponseEntity(exception.message, HttpStatus.NO_CONTENT)

}