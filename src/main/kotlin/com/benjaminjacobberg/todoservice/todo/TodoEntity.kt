/*
 * Copyright (c) Benjamin Jacob Berg <https://gitlab.com/benjamin.berg> and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package com.benjaminjacobberg.todoservice.todo

import org.springframework.web.bind.annotation.RequestParam
import java.util.*
import javax.persistence.ElementCollection
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class TodoEntity(@Id @GeneratedValue @RequestParam val id: UUID? = null,
                      @ElementCollection @RequestParam val items: List<String>)