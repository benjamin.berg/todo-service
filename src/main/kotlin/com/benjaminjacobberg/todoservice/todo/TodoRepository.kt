/*
 * Copyright (c) Benjamin Jacob Berg <https://gitlab.com/benjamin.berg> and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package com.benjaminjacobberg.todoservice.todo

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface TodoRepository : JpaRepository<TodoEntity, UUID>