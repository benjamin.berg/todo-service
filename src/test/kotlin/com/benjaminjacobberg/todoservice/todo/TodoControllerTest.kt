/*
 * Copyright (c) Benjamin Jacob Berg <https://gitlab.com/benjamin.berg> and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package com.benjaminjacobberg.todoservice.todo

import com.google.gson.Gson
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.*
import java.util.*


@SpringBootTest
@AutoConfigureMockMvc
internal class TodoControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var todoRepository: TodoRepository

    @BeforeEach
    fun setup() {
        todoRepository.deleteAll()
    }

    @AfterEach
    fun teardown() {
        todoRepository.deleteAll()
    }

    @Test
    fun `it should create an entity`() {
        val createTodoEntity = TodoEntity(items = listOf("Item 1", "Item 2", "Item 3"))
        val createTodoRequest = Gson().toJson(createTodoEntity)

        mockMvc.post("/todo") {
            contentType = MediaType.APPLICATION_JSON
            content = createTodoRequest
            accept = MediaType.APPLICATION_JSON
        }.andDo {
            print()
        }.andExpect {
            status { isOk() }
            content { contentType(MediaType.APPLICATION_JSON) }
            jsonPath("$.items[0]", `is`(createTodoEntity.items.first()))
            jsonPath("$.items[1]", `is`(createTodoEntity.items[1]))
            jsonPath("$.items[2]", `is`(createTodoEntity.items.last()))
        }
    }

    @Test
    fun `it should list filtered entities`() {
        val createTodoEntity = TodoEntity(items = listOf("Item 1", "Item 2", "Item 3"))
        val createTodoRequest = Gson().toJson(createTodoEntity)

        mockMvc.post("/todo") {
            contentType = MediaType.APPLICATION_JSON
            content = createTodoRequest
            accept = MediaType.APPLICATION_JSON
        }.andDo {
            print()
        }


        mockMvc.get("/todo?items=Item 1&items=Item 2&items=Item 3").andDo {
            print()
        }.andExpect {
            status { isOk() }
            content { contentType(MediaType.APPLICATION_JSON) }
            jsonPath("$.content[0].items[0]", `is`(createTodoEntity.items.first()))
            jsonPath("$.content[0].items[1]", `is`(createTodoEntity.items[1]))
            jsonPath("$.content[0].items[2]", `is`(createTodoEntity.items.last()))
        }
    }

    @Test
    fun `it should replace an existing entity`() {
        val createTodoEntity = TodoEntity(items = listOf("Item 1", "Item 2", "Item 3"))
        val createTodoRequest = Gson().toJson(createTodoEntity)

        val result = mockMvc.post("/todo") {
            contentType = MediaType.APPLICATION_JSON
            content = createTodoRequest
            accept = MediaType.APPLICATION_JSON
        }.andDo {
            print()
        }.andReturn()
        val resultTodoEntity = Gson().fromJson(result.response.contentAsString, TodoEntity::class.java)

        val updateTodoEntity = TodoEntity(id = resultTodoEntity.id, items = listOf("Item 1", "Item 2", "Item 3", "Item 4"))
        val updateTodoRequest = Gson().toJson(updateTodoEntity)

        mockMvc.put("/todo") {
            contentType = MediaType.APPLICATION_JSON
            content = updateTodoRequest
            accept = MediaType.APPLICATION_JSON
        }.andDo {
            print()
        }.andExpect {
            status { isOk() }
            content { contentType(MediaType.APPLICATION_JSON) }
            content { json(updateTodoRequest) }
        }
    }

    @Test
    fun `it should upsert if an entity isn't found to replace`() {
        val updateTodoEntity = TodoEntity(items = listOf("Item 1", "Item 2", "Item 3", "Item 4"))
        val updateTodoRequest = Gson().toJson(updateTodoEntity)

        mockMvc.put("/todo") {
            contentType = MediaType.APPLICATION_JSON
            content = updateTodoRequest
            accept = MediaType.APPLICATION_JSON
        }.andDo {
            print()
        }.andExpect {
            status { isOk() }
            content { contentType(MediaType.APPLICATION_JSON) }
            jsonPath("$.items[0]", `is`(updateTodoEntity.items.first()))
            jsonPath("$.items[1]", `is`(updateTodoEntity.items[1]))
            jsonPath("$.items[2]", `is`(updateTodoEntity.items[2]))
            jsonPath("$.items[3]", `is`(updateTodoEntity.items.last()))
        }
    }

    @Test
    fun `it should delete an existing entity`() {
        val createTodoEntity = TodoEntity(items = listOf("Item 1", "Item 2", "Item 3"))
        val createTodoRequest = Gson().toJson(createTodoEntity)

        val result = mockMvc.post("/todo") {
            contentType = MediaType.APPLICATION_JSON
            content = createTodoRequest
            accept = MediaType.APPLICATION_JSON
        }.andDo {
            print()
        }.andReturn()
        val resultTodoEntity = Gson().fromJson(result.response.contentAsString, TodoEntity::class.java)

        mockMvc.delete("/todo/" + resultTodoEntity.id) {
            contentType = MediaType.APPLICATION_JSON
            accept = MediaType.APPLICATION_JSON
        }.andDo {
            print()
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `it should return a 204 if an entity isn't found to delete`() {
        mockMvc.delete("/todo/" + UUID.randomUUID()) {
            contentType = MediaType.APPLICATION_JSON
            accept = MediaType.APPLICATION_JSON
        }.andDo {
            print()
        }.andExpect {
            status { isNoContent() }
        }
    }

}