/*
 * Copyright (c) Benjamin Jacob Berg <https://gitlab.com/benjamin.berg> and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package com.benjaminjacobberg.todoservice

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ApplicationTests {

    @Test
    fun contextLoads() {
    }

}
