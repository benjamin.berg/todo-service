#!/bin/sh

#
# Copyright (c) Benjamin Jacob Berg <https://github.com/benjaminjacobberg> and contributors. All rights reserved.
# Licensed under the MIT license. See LICENSE file in the project root for details.
#

# shellcheck disable=SC2039
# shellcheck disable=SC2034
for i in {1..1000}; do
    curl --header "Content-Type: application/json" \
  --request POST \
  --data '{ "items": ["1", "2", "3"] }' \
  http://localhost:8080/todo &&
  curl --compressed http://localhost:8080/todo
done